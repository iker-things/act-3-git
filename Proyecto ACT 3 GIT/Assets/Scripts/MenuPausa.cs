using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MenuPausa : MonoBehaviour
{
    [SerializeField] private GameObject botonPausa;

    [SerializeField] private GameObject panelPausa;
    public void Pausa()
    {
        Time.timeScale = 0f;
        botonPausa.SetActive(false);
        panelPausa.SetActive(true);
    }
    public void Continuar()
    {
        Time.timeScale = 1f;
        botonPausa.SetActive(true);
        panelPausa.SetActive(false);
    }
    public void Reiniciar()
    {

        Time.timeScale = 1f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Salir()
    {
        Debug.Log("Saliendo");
        SceneManager.LoadScene(0);
        Time.timeScale = 1f;
    }
}
